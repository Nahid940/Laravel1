@extends('admin.layout.master')

@section('table')

    @include('admin.ajaxModal')

    <button data-toggle="modal" data-target="#myModal" class="btn btn-info"> + Add new student</button>

    <table class="table table-striped table-hover ">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>

        <tbody id="tbody">
        @php
            $sl=1;
        @endphp

        @foreach($data->all() as $d)
        <tr class="success">
            <td>{{$sl++}}</td>
            <td>{{$d->name}}</td>
            <td>{{$d->email}}</td>

            <td><a href="{{url('admin/edit/'.$d->id)}}" class="btn btn-info">Edit</a></td>
            <td>
                {!! Form::open(['ulr'=>'']) !!}
                {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {{$data->links()}}

@endsection

@section('script')



    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


                $('#frm_insert').on('submit',function (e) {
                    e.preventDefault();
                    var data=$(this).serialize();
                    var url=$(this).attr('action');
                    var post=$(this).attr('method');

                    $.ajax({
                        type:post,
                        url:url,
                        data:data,
                        dataTy:'json',
                        success:function (data) {
                            $('#dataInsert').text('Data inserted !!');
                        },

                        error: function(data)
                        {
                            var errors = '';
                            for(datos in data.responseJSON){
                                errors += data.responseJSON[datos];
                            }
                            $('#dataInsert').show().html(errors);
                        }

                    });

                });


    </script>
@endsection