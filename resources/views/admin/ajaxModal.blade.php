<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add student from here</h4>
            </div>
            <div class="modal-body">
                <center><span id="dataInsert" class="label label-success" style="font-weight: bold;text-align: center;font-size: 20px"></span></center>

                @foreach($errors as $error)
                    <div class="alert alert-danger">{{$error}}</div>
                @endforeach




                <form action="{{\Illuminate\Support\Facades\URL::to('admin/storeDataByAjax')}}" id="frm_insert" method="post">
                    {{csrf_field()}}
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Name</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>

                    <div class="form-group has-success">
                        <label class="control-label" for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email">
                    </div>

                    <div class="form-group has-success">
                        <label class="control-label" for="age">Age</label>
                        <input type="text" class="form-control" id="age" name="age">
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" >Insert</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
</div>



