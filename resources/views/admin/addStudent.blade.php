@extends('admin.layout.master')

@section('content')


    @if(session('insert'))
        <div class="alert alert-success">{{session('insert')}}</div>
        @endif

    @foreach($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
        @endforeach

    <form class="form-horizontal" method="post" action="{{url('admin/store')}}">
        {{csrf_field()}}
        <fieldset>
            <legend>Add student info</legend>
            <div class="form-group">
                <label for="name" class="col-lg-2 control-label">Name</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="name" placeholder="Name" name="name">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputEmail" placeholder="Email" name="email">
                </div>
            </div>




            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </fieldset>
    </form>

    @endsection