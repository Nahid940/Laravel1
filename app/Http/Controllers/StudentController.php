<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('admin/addStudent');
    }


    public function view(){

        $data=Student::paginate(5);
        return view('admin/view',compact('data'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
        $valideData=$this->validate($request,[
            'name'=>'required',
            'email'=>'required',
        ]);
        Student::create($valideData);
        return redirect('admin/')->with('insert','Data inserted !!');

    }

    public function storeDataByAjax(Request $request){


        if($request->ajax()){

            $validData=$this->validate($request,[
                'name'=>'required',
                'email'=>'required',
                'age'=>'required',
            ]);

            Student::create($validData);

        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
