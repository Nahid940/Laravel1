<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/','StudentController@index');
Route::get('admin/view','StudentController@view');
Route::post('admin/store','StudentController@store');
Route::post('admin/storeDataByAjax','StudentController@storeDataByAjax');
